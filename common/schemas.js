Schemas = {};

Schemas.borrowerSchema = new SimpleSchema({
    fullName: {
        type: String
    },
    contactNumber: {
        type: String
    },
    borrowedOn: {
        type: Date
    }
});