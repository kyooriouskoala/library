Meteor.methods({
    addBorrower: function(doc) {
        if (! Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }
        Meteor.users.deny({
            insert: function() {
                return true;
            }
        });
        
        check(doc, Schemas.borrowerSchema);
        
        var myData = doc;

        //var bookID = Template.parentData(1)._id;
        //console.log("bookID: " + bookID);

        //myData["bookID"] = bookID;
        myData["createdBy"] = Meteor.userId();
        myData["createdAt"] = new Date();

        Borrowers.insert(myData);
    }
});